using UnityEngine;

namespace Handjob.Scripts.HelperScripts
{
    public class InstantiateHelper
    {
        public static Object InstantiateWithoutParent(GameObject original, Transform transform)
        {
            return Object.Instantiate(original, transform.position, transform.rotation);
        }
    }
}