﻿using UnityEngine;
using VRTK;

namespace Handjob.Scripts.InGameMenu
{
    public class MenuToggle : MonoBehaviour
    {
        [SerializeField] private VRTK_ControllerEvents _controllerEvents;
        [SerializeField] private GameObject menu;

        private bool menuState;

        private void OnEnable()
        {
            _controllerEvents.StartMenuPressed += ControllerEvents_StartMenuPressed;
            menuState = true;
            ToggleMenu();
        }

        private void OnDisable()
        {
            _controllerEvents.StartMenuPressed -= ControllerEvents_StartMenuPressed;
        }

        private void ControllerEvents_StartMenuPressed(object sender, ControllerInteractionEventArgs e)
        {
            ToggleMenu();
        }

        public void ToggleMenu()
        {
            menuState = !menuState;
            menu.SetActive(menuState);
        }
    }
}