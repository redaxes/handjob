﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Handjob.Scripts
{
    public class OutroPlayer : MonoBehaviour
    {

        public Light light;
        public Image canvasImage;
        private AnalogGlitch _analogGlitch;
        public float interval = 0.1f;
    
        // Start is called before the first frame update
        private void Start()
        {
            _analogGlitch = GetComponent<AnalogGlitch>();
            StartCoroutine(PlayerSequence());
        }

        private IEnumerator PlayerSequence()
        {
            yield return new WaitForSeconds(30);
            float deltaTime = 0;
            while (deltaTime < 3)
            {
                yield return new WaitForSeconds(interval);
                deltaTime += interval;
                _analogGlitch.scanLineJitter = deltaTime/3;
                _analogGlitch.colorDrift = deltaTime/3;
                light.intensity = deltaTime;
            }
            yield return new WaitForSeconds(2.5f);
            canvasImage.transform.localScale = Vector3.one;
            yield return  new WaitForSeconds(1);
            SceneManager.LoadScene(0);
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
