﻿using UnityEngine.SceneManagement;

namespace Handjob.Scripts.Menu
{
    public class PlayButton : UIItem
    {
        public override void Action()
        {
            SceneManager.LoadScene(1);
        }
    }
}
