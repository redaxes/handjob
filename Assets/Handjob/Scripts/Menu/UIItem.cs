﻿using TMPro;
using UnityEngine;
using VRTK;

namespace Handjob.Scripts.Menu
{
    public abstract class UIItem : MonoBehaviour
    {

        [FMODUnity.EventRef] public string StartHoverEvent;
        [FMODUnity.EventRef] public string ActionEvent;
    
        [SerializeField] protected Color inactiveColor = Color.white;
        [SerializeField] protected Color hoverColor = Color.cyan;
        [SerializeField] protected Color activeColor = Color.blue;
    
        protected TextMeshPro _tmp;
        protected Menu _menu;

        void Start()
        {
            _tmp = GetComponent<TextMeshPro>();
            _menu = FindObjectOfType<Menu>();
            StopHover();
        }

        public virtual void StopHover()
        {
            _tmp.color = inactiveColor;
            _menu.StopHover();
        }
    
        public virtual void Hover()
        {
            _tmp.color = hoverColor;
            _menu.MenuHover(this);
            FMODUnity.RuntimeManager.PlayOneShot(StartHoverEvent);
        }

        public virtual void ActivateButton()
        {
            _tmp.color = activeColor;
            FMODUnity.RuntimeManager.PlayOneShot(ActionEvent);
            Action();
        }

        public abstract void Action();

        private void OnTriggerEnter(Collider other)
        {
            if (other.transform.root.GetComponentInChildren<VRTK_ControllerEvents>() != null)
            {
                Hover();
            }
        }
    
        private void OnTriggerExit(Collider other)
        {
            if (other.transform.root.GetComponentInChildren<VRTK_ControllerEvents>() != null)
            {
                StopHover();
            }
        }
    }
}
