﻿using UnityEngine;

namespace Handjob.Scripts.Menu
{
    public class ExitButton : UIItem
    {
        public override void Action()
        {
            Application.Quit();
        }
    }
}
