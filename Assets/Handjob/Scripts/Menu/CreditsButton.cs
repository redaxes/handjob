﻿namespace Handjob.Scripts.Menu
{
    public class CreditsButton : UIItem
    {
        public override void Hover()
        {
            _tmp.color = hoverColor;
            _menu.ShowCredits(true);
        }

        public override void StopHover()
        {
            _tmp.color = inactiveColor;
            _menu.ShowCredits(false);
        }

        public override void Action()
        {
            throw new System.NotImplementedException();
        }
    }
}
