﻿using Handjob.Scripts.Extra;
using UnityEditor;
using UnityEngine;
using VRTK;

namespace Handjob.Scripts.Editor
{
    [CustomEditor(typeof(Shower))]
    public class ShowerEditor : UnityEditor.Editor
    {
        private Shower _target;

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
        
        
            if (GUILayout.Button("Start Shower (Only Play Mode)"))
            {
                if(Application.isPlaying) _target.StartShower();
            }
        
        }

        private void OnEnable()
        {
            //This will get the inspected Object in question, the parent of this Editor instance so to say
            _target = (Shower) target;
        }
    }
}