﻿using Handjob.Scripts.Extra;
using UnityEditor;
using UnityEngine;

namespace Handjob.Scripts.Editor
{
    [CustomEditor(typeof(Highlight))]
    public class HightlightEditor : UnityEditor.Editor
    {
        private Highlight _target;

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
        
            if (GUILayout.Button("No Border"))
            {
                _target.DeactivateBorder();
            }
        
            if (GUILayout.Button("Hightlight Border"))
            {
                _target.ActivateHighlightBorder();
            }
        
            if (GUILayout.Button("Can Grab Border"))
            {
                _target.ActivateCanGrabBorder();
            }
        }

        private void OnEnable()
        {
            //This will get the inspected Object in question, the parent of this Editor instance so to say
            _target = (Highlight) target;
        }
    }
}
