﻿using Handjob.Scripts.AI;
using UnityEditor;
using UnityEngine;

namespace Handjob.Scripts.Editor
{
    [CustomEditor(typeof(MySimpleAI))]
    public class MySimpleAIEditor : UnityEditor.Editor
    {
        private MySimpleAI _target;
        private bool showMoreDetails;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (_target.IsDead)
            {
                if (GUILayout.Button("Revive (only in Play Mode!)"))
                {
                    if (EditorApplication.isPlaying) _target.Revive();
                    else Debug.Log("This action can only be executed in Play mode!");
                }
            }
            else
            {
                if (GUILayout.Button("Die (only in Play Mode!)"))
                {
                    if (EditorApplication.isPlaying) _target.Die();
                    else Debug.Log("This action can only be executed in Play mode!");
                }
            }

        
            if (GUILayout.Button((showMoreDetails ? "Hide" : "Show") + " more details"))
            {
                showMoreDetails = !showMoreDetails;
            }
        
            if (showMoreDetails)
            {
                GUILayout.Label("State : " +  (_target.StateStringInfo ?? "Only available in Play Mode"));    
                GUILayout.Label("Animation Speed : " +  (_target.animationSpeedInfo ?? -1));  
            }
        
        }

        private void OnEnable()
        {
            //This will get the inspected Object in question, the parent of this Editor instance so to say
            _target = (MySimpleAI) target;
        }
    }
}
