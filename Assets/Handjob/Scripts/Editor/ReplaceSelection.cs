﻿/* This wizard will replace a selection with an object or prefab.
 * Scene objects will be cloned (destroying their prefab links).
 * Original coding by 'yesfish', nabbed from Unity Forums
 * 'keep parent' added by Dave A (also removed 'rotation' option, using localRotation
 */

using UnityEditor;
using UnityEngine;

namespace Handjob.Scripts.Editor
{
    public class ReplaceSelection : ScriptableWizard
    {
        private static GameObject replacement;
        private static bool keep;

        public GameObject ReplacementObject;
        public bool KeepOriginals;

        [MenuItem("GameObject/-Replace Selection...")]
        private static void CreateWizard()
        {
            DisplayWizard(
                "Replace Selection", typeof(ReplaceSelection), "Replace");
        }

        public ReplaceSelection()
        {
            ReplacementObject = replacement;
            KeepOriginals = keep;
        }

        private void OnWizardUpdate()
        {
            replacement = ReplacementObject;
            keep = KeepOriginals;
        }

        private void OnWizardCreate()
        {
            if (replacement == null)
                return;

            Undo.RegisterSceneUndo("Replace Selection");

            Transform[] transforms = Selection.GetTransforms(
                SelectionMode.TopLevel | SelectionMode.OnlyUserModifiable);

            foreach (Transform t in transforms)
            {
                GameObject g;
                PrefabType pref = EditorUtility.GetPrefabType(replacement);

                if (pref == PrefabType.Prefab || pref == PrefabType.ModelPrefab)
                {
                    g = (GameObject) EditorUtility.InstantiatePrefab(replacement);
                }
                else
                {
                    g = Instantiate(replacement);
                }

                Transform gTransform = g.transform;
                gTransform.parent = t.parent;
                g.name = replacement.name;
                gTransform.localPosition = t.localPosition;
                gTransform.localScale = t.localScale;
                gTransform.localRotation = t.localRotation;
            }

            if (!keep)
            {
                foreach (GameObject g in Selection.gameObjects)
                {
                    DestroyImmediate(g);
                }
            }
        }
    }
}