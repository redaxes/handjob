﻿using System;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using Random = UnityEngine.Random;

namespace Handjob.Scripts.Game
{
    public class SoundEventHandler : MonoBehaviour
    {

        public float teleportVolume = 0.5f;
    
        #region EventEmitter References

        private static SoundEventHandler _SoundEventHandler;
    
        //The VRTK_BasicTeleport has a callback function for when the Teleport is initiated.
        [SerializeField] private VRTK_BasicTeleport teleporterRef;
    
        #endregion
    
        #region FMOD Event References
    
        //Environment
        [FMODUnity.EventRef] public string TeleportSoundEvent = "";
        [FMODUnity.EventRef] public string ThrowSoundEvent = "";
        [FMODUnity.EventRef] public string PhysicsHitSoundEvent = "";
        [FMODUnity.EventRef] public string GrabSoundEvent = "";
        //UI
        [FMODUnity.EventRef] public string UIConfirmSoundEvent = "";
        [FMODUnity.EventRef] public string UIErrorSoundEvent = "";
        //Player
        [FMODUnity.EventRef] public string PlayerHandEquipEvent = "";
        [FMODUnity.EventRef] public string PlayerHandUnequipEvent = "";
        [FMODUnity.EventRef] public string PlayerShootEvent = "";
        [FMODUnity.EventRef] public string PlayerShootHitEvent = "";
        [FMODUnity.EventRef] public string PlayerShootRecallEvent = "";
        [FMODUnity.EventRef] public string PlayerCameraThrowEvent = "";
        [FMODUnity.EventRef] public string PlayerCameraLockEvent = "";
        [FMODUnity.EventRef] public string PlayerCameraExplodeEvent = "";
        [FMODUnity.EventRef] public string PlayerCameraINEvent = "";
        [FMODUnity.EventRef] public string PlayerCameraOUTEvent = "";
        [FMODUnity.EventRef] public string PlayerDuckEvent = "";
        //Enemy
        [FMODUnity.EventRef] public List<string> EnemyEvents;
    
    
    
        #endregion
    
        #region FMOD EventInstances
    
        FMOD.Studio.EventInstance teleportInstance;

        #endregion

        public delegate void SoundPhysicsInteractionEvent(PhysicsSoundEventType type, GameObject sender);
        public delegate void PlayerActionEvent(PlayerActionEventType type, GameObject sender);

        public enum PhysicsSoundEventType
        {
            Throw,
            PhysicsHit,
            Grab
        }

        public enum UISoundEventType
        {
            Confirm,
            Error,
            Accept
        }

        public enum PlayerActionEventType
        {
            Equip,
            Unequip,
            Shoot,
            Recall,
            ShootHit,
            CameraThrow,
            CameraLock,
            CameraExplode,
            CameraIN,
            CameraOUT,
            Duck
        }

        private SoundEventHandler()
        {
            _SoundEventHandler = this;
        }

        private void Awake()
        {
            _SoundEventHandler = this;
        }

        void Start()
        {
            teleporterRef.Teleporting += TeleportCallback;
            teleportInstance = FMODUnity.RuntimeManager.CreateInstance(TeleportSoundEvent);
            teleportInstance.setVolume(teleportVolume);
        }

        private void TeleportCallback(object sender, DestinationMarkerEventArgs e)
        {
            teleportInstance.start();
        }

        //Singleton Method
        public static SoundEventHandler GetSoundEventHandler()
        {
            return _SoundEventHandler;
        }

        public SoundPhysicsInteractionEvent GetSoundPhysicsInteractionEvent()
        {
            return OnSoundPhysicsInteraction;
        }
    
        private void OnSoundPhysicsInteraction(PhysicsSoundEventType type, GameObject sender)
        {
            Rigidbody rb = sender.GetComponent<Rigidbody>();
            float velocityMagnitudeVolume = 0.4f + (4.0f / rb.velocity.magnitude) * 0.2f;
        
            switch (type)
            {
                case PhysicsSoundEventType.Throw:
                    FMODUnity.RuntimeManager.PlayOneShotAttached(ThrowSoundEvent,sender,velocityMagnitudeVolume);
                    break;
                case PhysicsSoundEventType.PhysicsHit:
                    FMODUnity.RuntimeManager.PlayOneShotAttached(PhysicsHitSoundEvent,sender,velocityMagnitudeVolume);
                    break;
                case PhysicsSoundEventType.Grab:
                    FMODUnity.RuntimeManager.PlayOneShot(GrabSoundEvent, transform.position);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        public void PlayUISoundAttached(UISoundEventType type, GameObject attachTo)
        {
            switch(type)
            {
                case UISoundEventType.Confirm:
                    FMODUnity.RuntimeManager.PlayOneShotAttached(UIConfirmSoundEvent,attachTo);
                    break;
                case UISoundEventType.Error:
                    FMODUnity.RuntimeManager.PlayOneShotAttached(UIErrorSoundEvent,attachTo);
                    break;
                case UISoundEventType.Accept:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        public PlayerActionEvent GetPlayerActionEvent()
        {
            return OnPlayerActionEvent;
        }

        private void OnPlayerActionEvent(PlayerActionEventType type, GameObject sender)
        {
            switch (type)
            {
                case PlayerActionEventType.Equip:
                    FMODUnity.RuntimeManager.PlayOneShot(PlayerHandEquipEvent);
                    break;
                case PlayerActionEventType.Unequip:
                    FMODUnity.RuntimeManager.PlayOneShot(PlayerHandUnequipEvent);
                    break;
                case PlayerActionEventType.Shoot:
                    FMODUnity.RuntimeManager.PlayOneShot(PlayerShootEvent);
                    break;
                case PlayerActionEventType.ShootHit:
                    FMODUnity.RuntimeManager.PlayOneShotAttached(PlayerShootHitEvent,sender);
                    break;
                case PlayerActionEventType.Recall:
                    FMODUnity.RuntimeManager.PlayOneShot(PlayerShootRecallEvent);
                    break;
                case PlayerActionEventType.CameraThrow:
                    FMODUnity.RuntimeManager.PlayOneShotAttached(PlayerCameraThrowEvent,sender);
                    break;
                case PlayerActionEventType.CameraLock:
                    FMODUnity.RuntimeManager.PlayOneShotAttached(PlayerCameraLockEvent,sender);
                    break;
                case PlayerActionEventType.CameraExplode:
                    FMODUnity.RuntimeManager.PlayOneShotAttached(PlayerCameraExplodeEvent,sender);
                    break;
                case PlayerActionEventType.CameraIN:
                    FMODUnity.RuntimeManager.PlayOneShot(PlayerCameraINEvent);
                    break;
                case PlayerActionEventType.CameraOUT:
                    FMODUnity.RuntimeManager.PlayOneShot(PlayerCameraOUTEvent);
                    break;
                case PlayerActionEventType.Duck:
                    FMODUnity.RuntimeManager.PlayOneShotAttached(PlayerDuckEvent,sender);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        public void RandomEnemyVoice(GameObject sender)
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(EnemyEvents[Random.Range(0, EnemyEvents.Count)],sender);
        }

    }
}
