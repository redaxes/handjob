﻿
using UnityEngine;

namespace Handjob.Scripts.Player
{
    public class Damaged : MonoBehaviour
    {
        [SerializeField] private Player _player;
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Throw"))
            {
                _player.Damage();
            }
        }
    }
}
