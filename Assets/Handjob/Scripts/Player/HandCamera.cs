﻿using Handjob.Scripts.Game;
using UnityEngine;
using VRTK;

namespace Handjob.Scripts.Player
{
    public class HandCamera : AbstractHand
    {
        private ParticleSystem _effect;
        private Camera _main;
        private Camera _camera;
        private GameObject _mesh;
        private bool _isEffectPlaying;

        #region UnityMethodes

        /// <summary>
        /// Sets state and gets further more components
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            parent = transform.parent.parent;
            ChangeState(new Idle(this));
            _effect = GetComponentInChildren<ParticleSystem>();
            _mesh = transform.GetChild(1).gameObject;
            _camera = GetComponentInChildren<Camera>();
            _camera.gameObject.SetActive(false);

            _main = Camera.main;
        }

        /// <summary>
        /// Additional input for camera change
        /// </summary>
        /// <param name="handState"></param>
        protected override void ChangeState(HandState handState)
        {
            base.ChangeState(handState);
            if (vrtkControllerEvents != null)
            {
                vrtkControllerEvents.ButtonOneReleased -= ChangeCamera;
                vrtkControllerEvents.ButtonOneReleased += ChangeCamera;
            }
        }

        /// <summary>
        /// checks if main camera is the main and changes state after effect has stopped
        /// </summary>
        private void Update()
        {
            if (_main == null)
            {
                _main = Camera.main;
            }
            else if (_isEffectPlaying && !IsPlaying())
            {
                if (!_main.enabled)
                {
                    ChangeCamera();
                }

                _isEffectPlaying = false;
                _effect.Clear();
                transform.SetParent(GetParent().GetChild(1));

                transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
                transform.localPosition = Vector3.zero;
                _mesh.SetActive(true);
                _collider.enabled = false;
                ChangeState(new Shot(this));
            }
        }

        #endregion


        /// <summary>
        /// method is called when input was given to change camera
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeCamera(object sender, ControllerInteractionEventArgs e)
        {
            if (_state is Recall)
            {
                ChangeCamera();
            }
        }

        /// <summary>
        /// hand was attached
        /// </summary>
        /// <param name="getComponent"></param>
        public override void Attached(VRTK_ControllerEvents getComponent)
        {
            base.Attached(getComponent);
            _isEffectPlaying = false;
            ChangeState(new Shot(this));
        }

        /// <summary>
        /// hand was detached
        /// </summary>
        public override void Detached()
        {
            base.Detached();
            if (_state is Recall)
            {
                if (!_main.enabled)
                {
                    ChangeCamera();
                }
            }

            ResetPosition();
            ChangeState(new Idle(this));
        }

        /// <summary>
        /// Detaches the hand so its centers on belt position
        /// </summary>
        public override void DetachOnToBelt()
        {
            base.DetachOnToBelt();
            if (_state is Recall)
            {
                if (!_main.enabled)
                {
                    ChangeCamera();
                }
            }

            ResetPositionToZero();
            ChangeState(new OnBelt(this));
        }

        /// <summary>
        /// Changes the camera
        /// </summary>
        private void ChangeCamera()
        {
            _soundHandlerActionEvent?.Invoke(
                _main.gameObject.activeSelf
                    ? SoundEventHandler.PlayerActionEventType.CameraIN
                    : SoundEventHandler.PlayerActionEventType.CameraOUT, gameObject);
            _mesh.SetActive(!_mesh.activeSelf);
            _main.gameObject.SetActive(!_main.gameObject.activeSelf);
            _camera.gameObject.SetActive(!_camera.gameObject.activeSelf);
        }

        /// <summary>
        /// stops the hand and changes its state
        /// </summary>
        private void OnCollisionEnter()
        {
            if (_state is Stop)
            {
                StoppedHand();
                transform.localRotation = Quaternion.identity;
                ChangeState(new Recall(this));
            }
        }

        /// <summary>
        /// plays the effect
        /// </summary>
        private void PlayEffect()
        {
            _effect.Play(true);
            _soundHandlerActionEvent?.Invoke(SoundEventHandler.PlayerActionEventType.CameraExplode, gameObject);
            _isEffectPlaying = true;
        }

        /// <summary>
        /// resets to its default position
        /// </summary>
        public override void ResetPosition()
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }

        /// <summary>
        /// checks if effect is still playing
        /// </summary>
        /// <returns>boolean</returns>
        private bool IsPlaying()
        {
            return _effect.isPlaying;
        }

        /// <summary>
        /// shoots the camera forward
        /// </summary>
        public override void Shot()
        {
            _rigidbody.constraints = RigidbodyConstraints.None;
            _rigidbody.useGravity = true;
            transform.SetParent(null);
            _rigidbody.AddForce(parent.forward * force);
            _collider.enabled = true;
            _soundHandlerActionEvent?.Invoke(SoundEventHandler.PlayerActionEventType.CameraThrow, gameObject);
            ChangeState(new Stop(this));
        }

        /// <summary>
        /// starts the effect
        /// </summary>
        public override void Recall()
        {
            if (!IsPlaying())
            {
                _mesh.SetActive(false);
                PlayEffect();
            }
        }


        /// <summary>
        /// stops the hand at its position
        /// </summary>
        public override void Stop()
        {
            _soundHandlerActionEvent?.Invoke(SoundEventHandler.PlayerActionEventType.CameraLock, gameObject);
            StoppedHand();
            transform.localRotation = Quaternion.identity;
            ChangeState(new Recall(this));
        }
    }
}