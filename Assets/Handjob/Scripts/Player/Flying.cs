﻿using VRTK;

namespace Handjob.Scripts.Player
{
    public class Flying : HandState
    {
        public Flying(AbstractHand hand) : base(hand)
        {
        }

        public override void PerformAction(object sender, ControllerInteractionEventArgs e)
        {
            _hand.Fly();
        }
    }
}