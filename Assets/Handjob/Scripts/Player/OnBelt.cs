﻿using UnityEngine;
using VRTK;

namespace Handjob.Scripts.Player
{
    public class OnBelt : HandState
    {
        /// <summary>
        /// everything is frozen
        /// </summary>
        /// <param name="hand"></param>
        public OnBelt(AbstractHand hand) : base(hand)
        {
            _hand.GetRigidbody().constraints = RigidbodyConstraints.FreezeAll;
            _hand.GetRigidbody().useGravity = false;
            _hand.GetCollider().enabled = false;
            _hand.GetParent().GetComponent<Rigidbody>().useGravity = false;
            _hand.GetParent().GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        }

        /// <summary>
        /// calls the action which is to be made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void PerformAction(object sender, ControllerInteractionEventArgs e)
        {
            _hand.OnBelt();
        }
    }
}
