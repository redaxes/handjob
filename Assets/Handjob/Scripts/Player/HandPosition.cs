﻿using UnityEngine;

namespace Handjob.Scripts.Player
{
    public class HandPosition : MonoBehaviour
    {
        [Header("Mesh")] [SerializeField] private Transform rightMeshTransform;
        [SerializeField] private Transform leftMeshTransform;
        [Header("Slot")] [SerializeField] private Transform rightSlotTransform;
        [SerializeField] private Transform leftSlotTransform;

        public void SetPosition(bool isRightHand)
        {
            SetPosition(transform.GetChild(0), isRightHand ? rightMeshTransform : leftMeshTransform);
            if (rightSlotTransform != null && leftSlotTransform != null)
            {
                SetPosition(transform.GetChild(1), isRightHand ? rightSlotTransform : leftSlotTransform);
            }

        }

        private void SetPosition(Transform child, Transform mesh)
        {
            child.position = mesh.position;
            child.rotation = mesh.rotation;
        }
    }
}