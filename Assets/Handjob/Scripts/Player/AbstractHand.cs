﻿using Handjob.Scripts.Game;
using Handjob.Scripts.Interfaces;
using UnityEngine;
using VRTK;

namespace Handjob.Scripts.Player
{
    [RequireComponent(typeof(Rigidbody)), RequireComponent(typeof(Collider))]
    public abstract class AbstractHand : MonoBehaviour, IHandActions
    {
        [SerializeField] protected float force;

        protected VRTK_ControllerEvents vrtkControllerEvents;
        protected Transform parent;
        protected Rigidbody _rigidbody;
        protected HandState _state;
        protected Collider _collider;
        private VRTK_AvatarHandController _vrtkAvatarHandController;

        protected SoundEventHandler.PlayerActionEvent _soundHandlerActionEvent;
        private SoundEventHandler.SoundPhysicsInteractionEvent onSoundPhysicsInteractionEvent;

        #region UnityMethodes

        /// <summary>
        /// Get Important Components 
        /// </summary>
        protected virtual void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _collider = GetComponent<Collider>();
            parent = transform.parent;
        }

        private void Start()
        {
            _soundHandlerActionEvent = SoundEventHandler.GetSoundEventHandler().GetPlayerActionEvent();
            onSoundPhysicsInteractionEvent = SoundEventHandler.GetSoundEventHandler().GetSoundPhysicsInteractionEvent();
        }

        #endregion

        #region Attach/Detach


        /// <summary>
        /// Hand was attached
        /// </summary>
        /// <param name="getComponent"></param>
        public virtual void Attached(VRTK_ControllerEvents getComponent)
        {
            _soundHandlerActionEvent?.Invoke(SoundEventHandler.PlayerActionEventType.Equip,gameObject);
            vrtkControllerEvents = getComponent;
            _collider.enabled = false;
        }

        /// <summary>
        /// Hand was detached
        /// </summary>
        public virtual void Detached()
        {
            _soundHandlerActionEvent?.Invoke(SoundEventHandler.PlayerActionEventType.Unequip,gameObject);
            _collider.enabled = true;
        }

        public virtual void DetachOnToBelt()
        {
            _collider.enabled = true;
        }

        #endregion

        #region Getters

        /// <summary>
        /// Collider from the game object with the script
        /// </summary>
        /// <returns>Collider</returns>
        public Collider GetCollider()
        {
            return _collider;
        }

        /// <summary>
        /// Rigidbody from the game object width the script
        /// </summary>
        /// <returns>Rigidbody</returns>
        public Rigidbody GetRigidbody()
        {
            return _rigidbody;
        }

        #endregion

        #region AbstractMethodes

        #region InterfaceMethodes

        public abstract void Shot();
        public abstract void Recall();

        public void Idle()
        {
        }

        public abstract void Stop();

        public void OnBelt()
        {
        }

        public void Fly()
        {
        }

        #endregion

        public abstract void ResetPosition();

        #endregion

        /// <summary>
        /// Resets the hand so its stuck in one place.
        /// </summary>
        protected void StoppedHand()
        {
            _rigidbody.useGravity = false;
            _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.angularVelocity = Vector3.zero;
        }

        /// <summary>
        /// Changes the state of the hand
        /// </summary>
        /// <param name="handState">The hand itself</param>
        protected virtual void ChangeState(HandState handState)
        {
            if (_state != null)
            {
                if (vrtkControllerEvents != null)
                {
                    vrtkControllerEvents.TriggerPressed -= _state.PerformAction;
                }
            }

            _state = handState;
            if (vrtkControllerEvents != null)
            {
                vrtkControllerEvents.TriggerPressed += _state.PerformAction;
            }
        }

        /// <summary>
        /// Set the local position and rotation to zero
        /// </summary>
        protected virtual void ResetPositionToZero()
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }

        public HandState GetState()
        {
            return _state;
        }
        
        private void OnCollisionEnter()
        {
            onSoundPhysicsInteractionEvent?.Invoke(SoundEventHandler.PhysicsSoundEventType.PhysicsHit,gameObject);
        }

        public Transform GetParent()
        {
            return parent;
        }
    }
}