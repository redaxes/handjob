﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Handjob.Scripts.Player
{
    public class Player : MonoBehaviour
    {
        private int health;
        [SerializeField] private Wrist[] _wrists;

        private Camera _camera;
        private AnalogGlitch _analogGlitch;

        private void Awake()
        {
            _camera = Camera.main;
            health = 5;
            SetAnalogGlitch();
        }

        private void Update()
        {
            if (_camera == null)
            {
                _camera = Camera.main;
                SetAnalogGlitch();
            }
        }

        private void SetAnalogGlitch()
        {
            if (_camera != null)
            {
                _analogGlitch = _camera.gameObject.GetComponent<AnalogGlitch>();
            }
        }

        public void Damage()
        {
            _analogGlitch.colorDrift = 0.5f;
            _analogGlitch.scanLineJitter = 0.5f;
            StartCoroutine(StopGlitch());
            health -= 1;
            foreach (Wrist wrist in _wrists)
            {
                wrist.SetHealthUI(health);
            }
            if (health == 0)
            {
                Die();
            }
        }

        private IEnumerator StopGlitch()
        {
            yield return new WaitForSeconds(0.6f);
            _analogGlitch.colorDrift = 0;
            _analogGlitch.scanLineJitter = 0;
        }

        private void Die()
        {
            SceneManager.LoadScene(0);
        }
    }
}
