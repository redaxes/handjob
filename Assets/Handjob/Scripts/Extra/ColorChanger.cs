﻿using UnityEngine;

namespace Handjob.Scripts.Extra
{
    public class ColorChanger : MonoBehaviour
    {
        /// <summary>
        /// To use this script, the gameObject needs to have itself or children with 2 materials one is called "Primary"
        /// and the other "Secondary". You can also have more materials, but they will be ignored. To change the colors
        /// simply use ReplaceColors() with the respective rgb codes for the primary and then the secondary color
        /// </summary>
        private Material _primaryMaterial, _secondaryMaterial;
        private static readonly int Primary = Shader.PropertyToID("Primary");
        private static readonly int Secondary = Shader.PropertyToID("Secondary");
        private Color _primaryC, _secondaryC;
    
        // Start is called before the first frame update
        private void Start()
        {
            InstantiateMaterials();
            float r1 = Random.Range(0.0f, 1.0f),
                g1 = Random.Range(0.0f, 1.0f),
                b1 = Random.Range(0.0f, 1.0f),
                r2 = Random.Range(0.0f, 1.0f),
                g2 = Random.Range(0.0f, 1.0f),
                b2 = Random.Range(0.0f, 1.0f);
        
            ReplaceColors(r1,g1,b1,r2,g2,b2);
        }
    
        //main public function -------CALL THIS-------
        public void ReplaceColors(float r1, float g1, float b1, float r2, float g2, float b2)
        {
            _primaryC = new Color(r1, g1, b1);
            _secondaryC = new Color(r2, g2, b2);
       
            UpdateMaterials();
        }
        //main public function -------OR THIS-------
        public void ReplaceColors(Color primaryColor, Color secondaryColor)
        {
            float r1 = primaryColor.r;
            float g1= primaryColor.g;
            float b1 = primaryColor.b;
            float r2 = secondaryColor.r;
            float g2= secondaryColor.g;
            float b2 = secondaryColor.b;
        
            ReplaceColors(r1,g1,b1,r2,g2,b2);
        }

        //reassigns the colors on the materials
        private void UpdateMaterials()
        {
            _primaryMaterial.color = _primaryC;
            _secondaryMaterial.color = _secondaryC;
        }

        //goes through all the mesh renderers in all the children and replaces their materials if they are named primary or secondary
        private void InstantiateMaterials()
        {
            //normal shader
            _primaryMaterial = new Material(Shader.Find("Standard"));
            _secondaryMaterial = new Material(Shader.Find("Standard"));
        
            _primaryMaterial.name = "Primary";
            _secondaryMaterial.name = "Secondary";

            UpdateMaterials();
        
            MeshRenderer[] array = GetComponentsInChildren<MeshRenderer>();

            foreach (MeshRenderer renderer in array)
            {
                //Debug.Log("Found");
                //Debug.Log(renderer.material.name.ToString());
                if (renderer.material.name.ToString() == "Primary (Instance)")
                {
                    //Debug.Log("Primary");
                    renderer.material = _primaryMaterial;
                
                }
                else if (renderer.material.name.ToString() == "Secondary (Instance)")
                {
                    //Debug.Log("Secondary");
                    renderer.material = _secondaryMaterial;
                }
            }
        }
    
    }
}
