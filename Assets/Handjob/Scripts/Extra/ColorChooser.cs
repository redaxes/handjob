﻿using UnityEngine;

namespace Handjob.Scripts.Extra
{
    [RequireComponent(typeof(Collider))]
    public class ColorChooser : MonoBehaviour
    {
        private Color _primaryC, _secondaryC;
    
        // Start is called before the first frame update
        void Start()
        {
            InstantiateColors();
        }
    
        //when touched by a hand this replaces it's colors by this object's colors (primary / secondary)
        public void OnCollisionEnter(Collision other)
        {
            Debug.Log("Collision");
            //if other object is in hand layer
            if (other.collider.gameObject.layer == 11)
            {
                Debug.Log("Found");
                other.gameObject.GetComponent<ColorChanger>().ReplaceColors(_primaryC, _secondaryC);
            }
        }
    
        //look what the primary and secondary colors are and save them
        private void InstantiateColors()
        {
            MeshRenderer[] array = GetComponentsInChildren<MeshRenderer>();

            foreach (MeshRenderer renderer in array)
            {
                if (renderer.material.name.ToString() == "Primary (Instance)")
                {
                    _primaryC = renderer.material.color;
                }
                else if (renderer.material.name.ToString() == "Secondary (Instance)")
                {
                    _secondaryC = renderer.material.color;
                }
            }
        }
    }
}
