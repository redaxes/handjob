﻿using System.Collections;
using UnityEngine;

namespace Handjob.Scripts.Extra
{
    public class VendingMachine : MonoBehaviour
    {
        [FMODUnity.EventRef] public string rollEventRef;
        [FMODUnity.EventRef] public string openEventRef;
        [FMODUnity.EventRef] public string closeEventRef;
        private Animator _animator;
        private static readonly int OpenAnim = Animator.StringToHash("Open_Anim");
        private static readonly int RollAnim = Animator.StringToHash("Roll_Anim");

        private void Awake()
        {
            _animator = GetComponentInChildren<Animator>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                StartCoroutine(AnimateRobot());
            }
        }

        private IEnumerator AnimateRobot()
        {
            _animator.SetBool(OpenAnim, true);
            FMODUnity.RuntimeManager.PlayOneShotAttached(openEventRef, _animator.gameObject);
            yield return new WaitForSeconds(5);
            _animator.SetBool(RollAnim, true);
            FMODUnity.RuntimeManager.PlayOneShotAttached(rollEventRef, _animator.gameObject);
            yield return new WaitForSeconds(5);
            _animator.SetBool(RollAnim, false);
            yield return new WaitForSeconds(5);
            _animator.SetBool(OpenAnim, false);
            FMODUnity.RuntimeManager.PlayOneShotAttached(closeEventRef, _animator.gameObject);
        }
    }
}