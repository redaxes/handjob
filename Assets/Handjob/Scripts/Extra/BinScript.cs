﻿using UnityEngine;

namespace Handjob.Scripts.Extra
{
    public class BinScript : MonoBehaviour
    {
        [SerializeField] private ParticleSystem flame;

        [SerializeField] private AudioSource flameAudio;

        private void OnTriggerEnter(Collider other)
        {
            DestroyItemInBin(other, false);
        }

        private void DestroyItemInBin(Collider other, bool testing)
        {
            if (testing)
            {
                flame.Play();
                flameAudio.Play();
                //BinMaterial.set = Color.red;
            }
            else if (other.CompareTag("Grabbable"))
            {
                Destroy(other.gameObject);
                flame.Play();
                flameAudio.Play();
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                DestroyItemInBin(null, true);
            }
        }
    }
}