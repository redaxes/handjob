﻿using UnityEngine;

namespace Handjob.Scripts.Extra
{
    public class Highlight : MonoBehaviour
    {

        [SerializeField] private Material Material;
        [SerializeField] private Shader BorderShader;
        [SerializeField] private Shader DefaultShader;

        [SerializeField] [Range(1f, 5f)] private float BorderSize = 1.1f;

        [SerializeField] private Color HighlightColor;
        [SerializeField] private Color CanGrabColor;
    
        private static readonly int OutlineWidth = Shader.PropertyToID("_OutlineWidth");
        private static readonly int DistortColor = Shader.PropertyToID("_DistortColor");

        private void OnEnable()
        {
            Material.SetFloat(OutlineWidth,BorderSize);
        }

        public void DeactivateBorder()
        {
            Material.shader = DefaultShader;
        }

        public void ActivateHighlightBorder()
        {
            if(Material.shader != BorderShader) Material.shader = BorderShader;
            Material.SetColor(DistortColor,HighlightColor);
        }

        public void ActivateCanGrabBorder()
        {
            if(Material.shader != BorderShader) Material.shader = BorderShader;
            Material.SetColor(DistortColor,CanGrabColor);
        }
    
    }
}
