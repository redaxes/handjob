﻿using System.Collections;
using Handjob.Scripts.Game;
using UnityEngine;

namespace Handjob.Scripts.Door
{
    public class Door : MonoBehaviour
    {

        private Animator _animator;
        private static readonly int CharacterNearby = Animator.StringToHash("character_nearby");
        [SerializeField] private bool unlocked;
        [SerializeField] private int id;

        [SerializeField] private Room associatedRoomFront;
        [SerializeField] private Room associatedRoomBack;
        [SerializeField] private Transform associatedRoomFrontTransform;
        [SerializeField] private Transform associatedRoomBackTransform;

        private BoxCollider DoorHitZone;


        private void Start()
        {
            GameEvents.instance.onDoorWayTriggerEnter += Unlock;
            _animator = GetComponent<Animator>();
            DoorHitZone = GetComponent<BoxCollider>();
            DoorHitZone.enabled = false;
        }

        private void Unlock(int id)
        {
            if (id == this.id)
            {
                unlocked = true;
                DoorHitZone.enabled = true;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                if (unlocked && !_animator.GetBool(CharacterNearby))
                {
                    StopAllCoroutines();
                    _animator.SetBool(CharacterNearby,true);
                    EnableRooms();
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                if (unlocked && _animator.GetBool(CharacterNearby))
                {
                    _animator.SetBool(CharacterNearby,false);
                    StartCoroutine(OptimizeRooms(other));
                }
            }
        }

        private void EnableRooms()
        {
            associatedRoomBack.Enable();
            associatedRoomFront.Enable();
        }
        
        private IEnumerator OptimizeRooms(Component other)
        {
            yield return new WaitForSeconds(1.5f);
            if (Vector3.Distance(other.transform.position, associatedRoomFrontTransform.position) <
                Vector3.Distance(other.transform.position, associatedRoomBackTransform.position))
            {
                associatedRoomBack.Disable();
                associatedRoomFront.Enable();
            }
            else
            {
                associatedRoomBack.Enable();
                associatedRoomFront.Disable();
            }
        }
    }
}