﻿using Handjob.Scripts.Game;
using UnityEngine;

namespace Handjob.Scripts.Door
{
    public class DoorTrigger : MonoBehaviour
    {

        [SerializeField] private int id;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("KeyCard"))
            {
                if (other.GetComponent<KeyCard>().GetKeyCode() == id)
                {
                    SoundEventHandler.GetSoundEventHandler().PlayUISoundAttached(SoundEventHandler.UISoundEventType.Confirm,gameObject);
                    GameEvents.instance.DoorWayTriggerEnter(id);
                }
                else
                {
                    SoundEventHandler.GetSoundEventHandler().PlayUISoundAttached(SoundEventHandler.UISoundEventType.Error,gameObject);
                }
            }
        }
    }
}
