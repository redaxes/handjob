﻿using System.Collections;
using System.Collections.Generic;
using Handjob.Scripts.HelperScripts;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Handjob.Scripts.Conveyor
{
    public class ConveyorSpawner : MonoBehaviour
    {
    
        [SerializeField] private List<GameObject> spawnObjects;
        [SerializeField] private float spawnTimer;
        [SerializeField][Range(0,100)] int spawnProbability;


        private void Start()
        {
            StartCoroutine(Spawn());
        }

        private IEnumerator Spawn()
        {
            while (true)
            {
                if(Random.value*100 < spawnProbability) InstantiateHelper.InstantiateWithoutParent(spawnObjects[Random.Range(0, spawnObjects.Count)],transform);
            
                yield return new WaitForSeconds(spawnTimer);
            }
        }

        public void DisableSpawning()
        {
            StopAllCoroutines();
        }


    }
}
