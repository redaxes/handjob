﻿using UnityEngine;

namespace Handjob.Scripts.Conveyor
{
    public class Destroyable : MonoBehaviour
    {
        public bool DestroyEnabled { get; set; } = true;

        public void Destroy()
        {
            SpawnEffect e;
            if ((e = GetComponent<SpawnEffect>()) != null)
            {
                e.Destroy();
            }
            else Destroy(gameObject);
        }
    
    }
}
