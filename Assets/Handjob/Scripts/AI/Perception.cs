﻿using UnityEngine;

namespace Handjob.Scripts.AI
{
    public class Perception : MonoBehaviour
    {

        public bool drawGizmos;
        
        //I do not recommend setting the target here as it is always the player, best would be to make the Player accessible globally, or to have a sphereOverlap and check for tags.
        [SerializeField] private GameObject target;
        public int detectionRadius;
        public int attackRange;
        public int fovAngle;
        public LayerMask LayerMask;

        [SerializeField] public Transform raycastStart;

        private MySimpleAI ai;

        // Start is called before the first frame update
        private void Start()
        {
            if (!raycastStart)
            {
                raycastStart = transform;
                Debug.Log("RaycastStart has not been set for this AI Perception Script, is this intended?");
            }
        }

        public enum SeeResult
        {
            IN_RANGE,
            NOT_IN_RANGE,
            SEES_PLAYER,
            VISION_BLOCKED,
            IN_ATTACK_RANGE
        }

        public void See(MySimpleAI.SeeCallback cb)
        {
            SeeResult seeResult = SeeResult.NOT_IN_RANGE;
            Transform transformResult = null;
            //Needed Variables to deduct where the target is to you
            Vector3 eyes = raycastStart.transform.position;
            Vector3 difference = eyes - target.transform.position;
            float angle = Vector3.Angle(transform.forward, difference);

            if (angle >= 180 - fovAngle && difference.magnitude < detectionRadius)
            {
                if (Physics.Raycast(eyes, target.transform.position - eyes, out RaycastHit ray,
                    Mathf.Infinity, LayerMask))
                {
                    if (ray.distance < attackRange && ray.collider.gameObject.CompareTag("Player"))
                    {
                        seeResult = SeeResult.IN_ATTACK_RANGE;
                        transformResult = ray.transform;
                    }
                    else if (ray.collider.gameObject.CompareTag("Player"))
                    {
                        seeResult = SeeResult.SEES_PLAYER;
                        transformResult = ray.transform;
                    }
                    else seeResult = SeeResult.VISION_BLOCKED;
                }
            }
            else if (difference.magnitude < detectionRadius)
            {
                seeResult = SeeResult.IN_RANGE;
            }
            else
            {
                seeResult = SeeResult.NOT_IN_RANGE;
            }

            cb(seeResult, transformResult);
        }

        private void OnDrawGizmos()
        {
            if (drawGizmos && target)
            {
                //Needed Variables to deduct where the target is to you
                Vector3 center = transform.position;
                Vector3 difference = transform.position - target.transform.position;
                float angle = Vector3.Angle(transform.forward, difference);
                //Draw the Sphere showing the detection radius
                Gizmos.DrawWireSphere(transform.position, detectionRadius);
                //Draw the FOV
                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(transform.position, attackRange);
                Gizmos.DrawLine(center,
                    center + Quaternion.Euler(0, fovAngle, 0) * transform.forward * detectionRadius);
                Gizmos.DrawLine(center,
                    center + Quaternion.Euler(0, -fovAngle, 0) * transform.forward * detectionRadius);

                //Function Call, It determined the Color and the Destination of the Line, for vision obstruction!
                Gizmos.DrawLine(raycastStart.position, ChangeLineColorToPlayerAdvanced(angle, difference));
                // Gizmos.DrawRay(raycastStart.position,target.transform.position - raycastStart.position);

                //For extra info
                /*
                Gizmos.color = Color.yellow;
                Vector3 lookat = Vector3.RotateTowards(transform.forward,  (target.transform.position - transform.position).normalized , Mathf.PI, 0f);
                Gizmos.DrawLine(Vector3.zero, transform.forward*5);
              //  Gizmos.DrawLine(Vector3.zero, (target.transform.position - center).normalized*5);
                Gizmos.DrawLine(Vector3.zero,transform.position+lookat*5);
                */
            }
        }

        /*
        private void ChangeLineColorToPlayerSimple(float angle, Vector3 difference, Vector3 center)
        {
            //Change Line Color depending on if you see the Player or not
            if (angle >= 180 - fovAngle && difference.magnitude < detectionRadius) 
            { 
                Gizmos.color = Color.red; 
            }
            else if(difference.magnitude < detectionRadius)
            {
                Gizmos.color = Color.magenta;
            }
            else Gizmos.color = Color.gray;
        }
    */

        private Vector3 ChangeLineColorToPlayerAdvanced(float angle, Vector3 difference)
        {
            //Change Line Color depending on if you see the Player or not

            if (angle >= 180 - fovAngle && difference.magnitude < detectionRadius)
            {
                if (Physics.Raycast(raycastStart.position, target.transform.position - raycastStart.position, out RaycastHit ray,
                    detectionRadius, LayerMask))
                {
                    Gizmos.color = ray.collider.gameObject.CompareTag("Player") ? Color.red : Color.green;

                    return ray.point;
                }
            }
            else if (difference.magnitude < detectionRadius)
            {
                Gizmos.color = Color.magenta;
            }
            else Gizmos.color = Color.gray;

            return target.transform.position;
        }
    }
}