using System;
using Handjob.Scripts.Game;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Handjob.Scripts.AI
{
    public class AIIdlingState : AIState
    {
        private int MaxIdlingTime;
        private int MinIdlingTime;
        private float IdlingTime;
        private int VoiceLineProbability;

        private Vector3 InitialDestination;
        private bool returnInitialDestination;
        
        public AIIdlingState(MySimpleAI parent, int minIdle = 5, int maxIdle = 10, int voiceLineProbability = 50) : base(parent)
        {
            MinIdlingTime = minIdle;
            MaxIdlingTime = maxIdle;
            IdlingTime = MaxIdlingTime;
            VoiceLineProbability = voiceLineProbability;
        }

        public override bool Update(Perception.SeeResult seeResult)
        {
            bool stateChanged = false;
            if (!isActive) Activate();
            switch (seeResult)
            {
                case Perception.SeeResult.SEES_PLAYER:
                    stateChanged = ChangeState(ai.GetAIFollowingState());
                    break;
                case Perception.SeeResult.IN_ATTACK_RANGE:
                    stateChanged = ChangeState(ai.GetAIAttackingState());
                    break;
                case Perception.SeeResult.IN_RANGE:
                    break;
                case Perception.SeeResult.NOT_IN_RANGE:
                    break;
                case Perception.SeeResult.VISION_BLOCKED:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(seeResult), seeResult, null);
            }
            //Timer
            if (IdlingTime > 0) IdlingTime -= Time.deltaTime;
            else stateChanged = ChangeState(ai.GetAIPathState());
            destination = returnInitialDestination ? InitialDestination : ai.transform.position;
            return stateChanged;
        }

        public override Vector3 GetNextPoint()
        {
            return destination;
        } 

        protected override void Activate()
        {
            returnInitialDestination = false;
            InitialDestination = VectorHelper.Copy(ai.GetNavMeshAgent().destination);
            IdlingTime = Random.Range(MinIdlingTime, MaxIdlingTime);
            ai.ChangeAnimationState(-1);
            if(Random.value * 100 <= VoiceLineProbability) SoundEventHandler.GetSoundEventHandler().RandomEnemyVoice(ai.gameObject);
            base.Activate();
        }

        protected override void DeActivate()
        {
            returnInitialDestination = true;
            ai.GetNavMeshAgent().destination = InitialDestination;
            IdlingTime = MaxIdlingTime;
            base.DeActivate();
        }
    }
}