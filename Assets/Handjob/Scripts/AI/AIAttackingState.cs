using UnityEngine;

namespace Handjob.Scripts.AI
{
    public class AIAttackingState : AIState
    {
        private float attackTimer;
        private float attackCooldown = 5;

        private bool changeState;
        private bool changeStateAllowed;
        
        private bool attacking;

        public AIAttackingState(MySimpleAI parent) : base(parent)
        {
            hasAction = true;
            specialAnimation = 4;
        }

        public override bool Update(Perception.SeeResult seeResult)
        {
            if (changeState)
            {
                if (changeStateAllowed && !attacking)
                {
                    return ChangeState(ai.GetAIFollowingState());
                }
            }
            else changeState = seeResult != Perception.SeeResult.IN_ATTACK_RANGE;
            destination = ai.transform.position;
            return false;
        }

        public override Vector3 GetNextPoint()
        {
            return destination;
        }

        public override void Action()
        {
            if (!attacking)
            {
                if (changeState)
                {
                    changeStateAllowed = true;
                }
                else
                {
                    ai.ChangeAnimationState(specialAnimation);
                    attacking = true;
                }
            }
            else
            {
                attackTimer-= Time.deltaTime;
                if (attackTimer < 0)
                {
                    attacking = false;
                    attackTimer = attackCooldown;
                }
            }
        }

        protected override void Activate()
        {
            changeState = false;
            attacking = false;
            base.Activate();
        }

        protected override void DeActivate()
        {
            changeState = false;
            attacking = false;
            base.DeActivate();
        }
    }
}