using UnityEngine;

namespace Handjob.Scripts.AI
{
    public static class VectorHelper
    {

        public static Vector3 Copy(Vector3 v)
        {
            return new Vector3(v.x,v.y,v.z);
        }
    }
}