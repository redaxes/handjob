using UnityEngine;

namespace Handjob.Scripts.AI
{
    public interface IAIState
            {
        /// <summary>
        /// Update the state with visual information so it can calculate the destination etc.
        /// </summary>
        /// <returns> Boolean returns whether the state changed or not </returns>
        bool Update(Perception.SeeResult seeResult);

        /// <summary>
        /// Movement Related function used by MyAIState in order to know where to go next.
        /// </summary>
        /// <returns></returns>
        Vector3 GetNextPoint();
        /// <summary>
        /// Function called by MyAiState to notify that an animation event has been triggered. Used if anything happens to a state if an animation is done playing.
        /// </summary>
        /// <param name="state"> The animation state index </param>
        /// <param name="isRoot"> Whether root motion is applied, implies whether the animation was locomotive or static. </param>
        void AnimationEvent(int state, bool isRoot);

        /// <summary>
        /// State specific action executed every update tick.
        /// </summary>
        void Action();
    }

    
    /// <summary>
    /// Abstract class implementing the Interface
    /// </summary>
    public abstract class AIState : IAIState
    {
        /// <summary>
        /// All AIStates keep a reference to the ai for helper functions and getters.
        /// </summary>
        protected MySimpleAI ai;
        /// <summary>
        /// A state is inactive if it's not the current state in the ai. It needs this so it can trigger the Activate() function which reinitialize values depending on the state.
        /// </summary>
        protected bool isActive;
        /// <summary>
        /// The destination of this state for the navmeshagent.
        /// </summary>
        protected Vector3 destination;

        /// <summary>
        /// Whether this state also has a special action or not.
        /// </summary>
        protected bool hasAction = false;
        /// <summary>
        /// Public readonly var to check if the Action() method has to be executed or not.
        /// </summary>
        public bool HasAction => hasAction;

        /// <summary>
        /// If the State has a specific animation it wants to play it can check this bool whether it should wait.
        /// </summary>
        protected bool animEnded;
        
        /// <summary>
        /// Defaultanimation is movement or idle most of the time and specialanimations are oneshots specific to the state.
        /// </summary>
        protected int defaultAnimation = 1;
        protected int specialAnimation;

        /// <summary>
        /// Constructor every state should inherit
        /// </summary>
        /// <param name="parent"> The ai parent </param>
        protected AIState(MySimpleAI parent)
        {
            ai = parent;
        }

        public abstract bool Update(Perception.SeeResult seeResult);

        /// <summary>
        /// Function returning where the navmeshagent should go to as his next destination.
        /// </summary>
        /// <returns></returns>
        public abstract Vector3 GetNextPoint();

        /// <summary>
        /// Basic animationevent definition, most of the time it's enough to keep this default.
        /// </summary>
        /// <param name="state"> Animation state </param>
        /// <param name="isRoot"> Whether root motion is applied, implies whether the animation was locomotive or static.
        ///                       Used to determine if animation started or ended.</param>
        public void AnimationEvent(int state, bool isRoot)
        {
            if (state == specialAnimation)
            {
                if (!isRoot)
                {
                    animEnded = true;
                    ai.ChangeAnimationState(defaultAnimation);
                }
            }
            else ai.ChangeAnimationState(defaultAnimation);
        }

        /// <summary>
        /// A sample definition is provided so child classes are not forced to execute this.
        /// </summary>
        public virtual void Action()
        {
        }

        /// <summary>
        /// Simple helper method to change the State
        /// </summary>
        /// <param name="next"> Reference to next State </param>
        /// <returns></returns>
        protected bool ChangeState(AIState next)
        {
            ai.ChangeState(next);
            DeActivate();
            return true;
        }
        
        
        protected virtual void Activate()
        {
            isActive = true;
        }

        protected virtual void DeActivate()
        {
            isActive = false;
        }
    }
}