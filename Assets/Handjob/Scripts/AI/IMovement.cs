﻿using UnityEngine;

namespace Handjob.Scripts.AI
{
    public interface IMovement
    {
        void MoveTo(Vector3 targetPosition);

        void RotateTo(Vector3 targetPosition);

        void RotateTo(Quaternion quaternion);
    }
}
