﻿using UnityEngine;

namespace Handjob.Scripts.AI
{
    public class AnimEventReceiver : MonoBehaviour
    {

        private MySimpleAI ai;
        private MySimpleAI.OnAnimationEvent _onAnimationEvent;
    
        // Start is called before the first frame update
        private void Start()
        {
            ai = GetComponentInParent<MySimpleAI>();
            _onAnimationEvent = ai.AnimationEvent;
        }

        public void AnimationEvent(float speed)
        {
            _onAnimationEvent(speed);
        }

        public void ThrowBegin()
        {
            ai.ThrowBegin();
        }

        public void ThrowEnd()
        {
            ai.ThrowEnd();
        }
    
    
    }
}
