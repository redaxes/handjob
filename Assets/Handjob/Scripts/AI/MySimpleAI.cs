﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Handjob.Scripts.AI
{
    public class MySimpleAI : MonoBehaviour
    {

        #region Public Info Vars

        public string StateStringInfo => state?.ToString();
        public float?  animationSpeedInfo => _animator != null ? _animator.speed : (float?) null;

        #endregion

        #region Private Vars

        private AIState state;
        private Perception _perception;
        private AIMovement _movement;
        private NavMeshAgent _navMeshAgent;
        private Animator _animator;
        private GameObject throwObject;

        public bool IsDead { get; private set; }

        private AIState AIFollowingState;
        private AIState AIPathState;
        private AIState AIPatrollingState;
        private AIState AIAttackingState;
        private AIState AIIdlingState;

        #endregion

        #region Visible Inspector Vars

        [SerializeField] private List<Transform> path = new List<Transform>();
        [SerializeField] private float minPathPointDistance = 1f;
        [SerializeField] private GameObject lastKnownPositionObject;
        [SerializeField] private int MinIdleTime = 2;
        [SerializeField] private int MaxIdleTime = 10;
        [SerializeField] private Transform throwObjectPos;
        [SerializeField][Range(0,100)] private int voiceLineProbability; 

        #endregion

        #region Animator Vars

        private static readonly int State = Animator.StringToHash("State");
        private static readonly int Speed = Animator.StringToHash("Speed");
        private static readonly int Turn = Animator.StringToHash("Turn");
        private static readonly int Rand = Animator.StringToHash("Random");

        #endregion

        #region Delegates

        public delegate void OnAnimationEvent(float speed);

        public delegate void SeeCallback(Perception.SeeResult seeResult, Transform transform);

        #endregion

        /// <summary>
        /// In Awake we get our refs to our components
        /// </summary>
        private void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _perception = GetComponent<Perception>();
            _animator = GetComponentInChildren<Animator>();
        }

        /// <summary>
        /// In Start we initialize our states so we can switch between them later on
        /// </summary>
        private void Start()
        {
            AIFollowingState = new AIFollowingState(this, lastKnownPositionObject);
            AIPathState = new AIPathingState(this);
            AIPatrollingState = new AIPatrollingState(this, lastKnownPositionObject);
            AIAttackingState = new AIAttackingState(this);
            AIIdlingState = new AIIdlingState(this,MinIdleTime,MaxIdleTime,voiceLineProbability);
            state = AIPathState;
            lastKnownPositionObject.transform.parent = null;
            throwObject = throwObjectPos.transform.GetChild(0).gameObject;
            throwObject.SetActive(false);
            if (path.Count < 1)
            {
                path.Add(transform);
                MinIdleTime = 15;
                MaxIdleTime = 15;
            }
        }

        /// <summary>
        /// In update we use our perception to see and process the visual information and react depending on the state.
        /// </summary>
        private void Update()
        {
            if (!IsDead)
            {
                //_perception is our perception component and OnSee our callback for when he's done seeing
                _perception.See(OnSee);
                //some states have their own actions next to moving, these are executed here
                if(state.HasAction) state.Action();
            }
        }
    
        /// <summary>
        /// Function called when the Enemy should die
        /// </summary>
        public void Die() 
        {
            //We disable the animator so it doesn't constrain the ragdoll anymore and the ragdoll does its thing
            _animator.enabled = false;
            IsDead = true;
        }

        /// <summary>
        /// Only for testing purposes
        /// </summary>
        public void Revive()
        {
            _animator.enabled = true;
            IsDead = false;
        }

        /// <summary>
        /// Simple helper method so that the states can tell the AI which state to change to.
        /// </summary>
        /// <param name="state"> Which state to change to </param>
        public void ChangeState(AIState state)
        {
            this.state = state;
        }

        /// <summary>
        /// Simple helper method so that states can control animation
        /// </summary>
        /// <param name="state"> Which animstate to change to, they are hardcoded in the animation tree </param>
        public void ChangeAnimationState(int state)
        {
            _animator.SetInteger(State, state);
        }

        /// <summary>
        /// Callback for the Perception Component, here we process the visual information given
        /// </summary>
        /// <param name="seeResult"></param>
        /// <param name="targetTransform"></param>
        private void OnSee(Perception.SeeResult seeResult, Transform targetTransform)
        {
            switch (seeResult)
            {
                case Perception.SeeResult.IN_ATTACK_RANGE:
                case Perception.SeeResult.SEES_PLAYER:
                    lastKnownPositionObject.SetActive(true);
                    lastKnownPositionObject.transform.position = new Vector3(targetTransform.position.x,
                        targetTransform.position.y, targetTransform.position.z);
                    break;
            }

            //Update the state with visual information, repeats as long as state changes occur.
            bool stateChanged;
            do
            {
                stateChanged = state.Update(seeResult);
            } while (stateChanged);
        
            state.Action();
        
            //Here we set the navmeshagent destination to whatever the current state says our destination is, the code in GetNextPoint varies dependent on the state
            Vector3 dest = state.GetNextPoint();
            if(dest != _navMeshAgent.destination) _navMeshAgent.SetDestination(dest);
        
            Vector3 s = _navMeshAgent.transform.InverseTransformDirection(_navMeshAgent.velocity).normalized;
            float speed = s.z * _navMeshAgent.speed;
            float turn = s.x * _navMeshAgent.angularSpeed;
        
            _animator.SetFloat(Speed, speed);
            _animator.SetFloat(Turn, turn);
            _animator.SetInteger(Rand,Random.Range(0, 100));
        }

        /// <summary>
        /// Simple helper method so that states can check whether a point was reached or not
        /// </summary>
        /// <param name="target"> Whether we use the lastknownposition object's position or the navmeshagent's destination for the check.
        ///     This is important because the following state's last steps are done based on memory, he follows until he's at the last position he knows about you.</param>
        /// <returns></returns>
        public bool PointReached(bool target)
        {
            if (target)
                return Vector3.Distance(transform.position, lastKnownPositionObject.transform.position) <
                       minPathPointDistance * 2;
            else return Vector3.Distance(transform.position, _navMeshAgent.destination) < minPathPointDistance;
        }
    
        /// <summary>
        /// Animation Event forwarded by the AnimEventReceiver
        /// </summary>
        /// <param name="speed"> The speed indicates whether the animation is movement based or not and gives us information about the current animation state.</param>
        public void AnimationEvent(float speed)
        {
            _animator.applyRootMotion = speed == 0;
            _navMeshAgent.speed = speed;
            if (!IsAnimPlaying()) state.AnimationEvent(_animator.GetInteger(State), _animator.applyRootMotion);
        }

        /// <summary>
        /// Simple helper method telling us if a static animation is playing.
        /// </summary>
        /// <returns></returns>
        private bool IsAnimPlaying()
        {
            return _navMeshAgent.speed == 0;
        }

        public void ThrowBegin()
        {
            throwObject.SetActive(true);
            throwObject.GetComponent<Throw>().targetPosition =
                VectorHelper.Copy(lastKnownPositionObject.transform.position);
        }

        public void ThrowEnd()
        {
            throwObject.GetComponent<Throw>().Release();
        }

        #region Getters and Setters

        public AIState GetAIFollowingState()
        {
            return AIFollowingState;
        }

        public AIState GetAIPathState()
        {
            return AIPathState;
        }

        public AIState GetAIPatrollingState()
        {
            return AIPatrollingState;
        }

        public AIState GetAIAttackingState()
        {
            return AIAttackingState;
        }
    
        public AIState GetAIIdlingState()
        {
            return AIIdlingState;
        }

        public NavMeshAgent GetNavMeshAgent()
        {
            return _navMeshAgent;
        }
    
        public List<Transform> GetPath()
        {
            return path;
        }

        public Transform GetPathAt(int index)
        {
            return path[index];
        }

        #endregion

    }
}