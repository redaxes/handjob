using System;
using UnityEngine;

namespace Handjob.Scripts.AI
{
    public class AIPathingState : AIState
    {
        private int patrolIndex;
        private bool idled;

        public AIPathingState(MySimpleAI parent) : base(parent)
        {
        }

        public override bool Update(Perception.SeeResult seeResult)
        {
            bool stateChanged = false;
            if (!isActive) Activate();
            switch (seeResult)
            {
                case Perception.SeeResult.SEES_PLAYER:
                    stateChanged = ChangeState(ai.GetAIFollowingState());
                    break;
                case Perception.SeeResult.IN_ATTACK_RANGE:
                    stateChanged = ChangeState(ai.GetAIAttackingState());
                    break;
                case Perception.SeeResult.IN_RANGE:
                    break;
                case Perception.SeeResult.NOT_IN_RANGE:
                    break;
                case Perception.SeeResult.VISION_BLOCKED:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(seeResult), seeResult, null);
            }

            if (ai.PointReached(false))
            {
                switch (patrolIndex)
                {
                    case 0 when !idled:
                        idled = true;
                        stateChanged = ChangeState(ai.GetAIIdlingState());
                        destination = ai.transform.position;
                        return stateChanged;
                    case 0 when idled:
                        idled = false;
                        break;
                }

                patrolIndex = (patrolIndex + 1) % ai.GetPath().Count;
            }
            destination = ai.GetPathAt(patrolIndex).position;
            return stateChanged;
    }

        public override Vector3 GetNextPoint()
        {
            return destination;
        }

        protected override void Activate()
        {
            ai.ChangeAnimationState(1); 
            base.Activate();
        }
    }
}