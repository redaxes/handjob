﻿using UnityEngine;

namespace Handjob.Scripts.AI
{
    public class RobotSphere : MonoBehaviour
    {
        // Start is called before the first frame update
        private void Start()
        {
            transform.Rotate(new Vector3(0,90,0));
        }

        // Update is called once per frame
        private void Update()
        {
            transform.Rotate(new Vector3(0,90,0));
        }
    }
}
