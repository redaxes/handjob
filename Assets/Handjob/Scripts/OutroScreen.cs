﻿using System.Collections;
using UnityEngine;
using UnityEngine.Video;

namespace Handjob.Scripts
{
    public class OutroScreen : MonoBehaviour
    {

        private VideoPlayer _player;
    
        // Start is called before the first frame update
        void Start()
        {
            _player = GetComponent<VideoPlayer>();
            StartCoroutine(OutroVideo());
        }

        private IEnumerator OutroVideo()
        {
            yield return new WaitForSeconds(11);
            _player.Play();
        }
    

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
