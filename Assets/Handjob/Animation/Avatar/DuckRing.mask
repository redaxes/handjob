%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: DuckRing
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: l_hand_world
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_grip
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_hand_ignore
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_index1
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_index1/b_l_index2
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_index1/b_l_index2/b_l_index3
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_index1/b_l_index2/b_l_index3/b_l_index_ignore
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_index1/b_l_index2/b_l_index3/phand_knuckle5
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_index1/b_l_index2/b_l_index3/phand_knuckle5/phand_fingerpart
      6
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_index1/b_l_index2/phand_knuckle4
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_index1/b_l_index2/phand_knuckle4/phand_fingerpart
      2
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_index1/phand_knuckle
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_index1/phand_knuckle/phand_fingerpart
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_middle1
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_middle1/b_l_middle2
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_middle1/b_l_middle2/b_l_middle3
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_middle1/b_l_middle2/b_l_middle3/b_l_middle_ignore
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_middle1/b_l_middle2/b_l_middle3/phand_knuckle7
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_middle1/b_l_middle2/b_l_middle3/phand_knuckle7/phand_fingerpart
      7
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_middle1/b_l_middle2/phand_knuckle6
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_middle1/b_l_middle2/phand_knuckle6/phand_fingerpart
      3
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_middle1/phand_knuckle1
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_middle1/phand_knuckle1/phand_fingerpart1
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_pinky0
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_pinky0/b_l_pinky1
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_pinky0/b_l_pinky1/b_l_pinky2
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_pinky0/b_l_pinky1/b_l_pinky2/b_l_pinky3
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_pinky0/b_l_pinky1/b_l_pinky2/b_l_pinky3/b_l_pinky_ignore
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_pinky0/b_l_pinky1/b_l_pinky2/b_l_pinky3/phand_knuckle14
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_pinky0/b_l_pinky1/b_l_pinky2/b_l_pinky3/phand_knuckle14/phand_fingerpart
      11
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_pinky0/b_l_pinky1/b_l_pinky2/phand_knuckle10
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_pinky0/b_l_pinky1/b_l_pinky2/phand_knuckle10/phand_fingerpart
      8
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_pinky0/b_l_pinky1/phand_knuckle3
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_pinky0/b_l_pinky1/phand_knuckle3/phand_fingerpart3
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_ring1
    m_Weight: 1
  - m_Path: l_hand_world/b_l_hand/b_l_ring1/b_l_ring2
    m_Weight: 1
  - m_Path: l_hand_world/b_l_hand/b_l_ring1/b_l_ring2/b_l_ring3
    m_Weight: 1
  - m_Path: l_hand_world/b_l_hand/b_l_ring1/b_l_ring2/b_l_ring3/b_l_ring_ignore
    m_Weight: 1
  - m_Path: l_hand_world/b_l_hand/b_l_ring1/b_l_ring2/b_l_ring3/phand_knuckle9
    m_Weight: 1
  - m_Path: l_hand_world/b_l_hand/b_l_ring1/b_l_ring2/b_l_ring3/phand_knuckle9/phand_fingerpart
      9
    m_Weight: 1
  - m_Path: l_hand_world/b_l_hand/b_l_ring1/b_l_ring2/phand_knuckle8
    m_Weight: 1
  - m_Path: l_hand_world/b_l_hand/b_l_ring1/b_l_ring2/phand_knuckle8/phand_fingerpart
      4
    m_Weight: 1
  - m_Path: l_hand_world/b_l_hand/b_l_ring1/phand_knuckle2
    m_Weight: 1
  - m_Path: l_hand_world/b_l_hand/b_l_ring1/phand_knuckle2/phand_fingerpart2
    m_Weight: 1
  - m_Path: l_hand_world/b_l_hand/b_l_thumb1
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_thumb1/b_l_thumb2
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_thumb1/b_l_thumb2/b_l_thumb3
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_thumb1/b_l_thumb2/b_l_thumb3/b_l_thumb_ignore
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_thumb1/b_l_thumb2/b_l_thumb3/phand_knuckle13
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_thumb1/b_l_thumb2/b_l_thumb3/phand_knuckle13/phand_fingerpart
      10
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_thumb1/b_l_thumb2/phand_knuckle12
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_thumb1/b_l_thumb2/phand_knuckle12/phand_fingerpart
      5
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_thumb1/phand_knuckle11
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/b_l_thumb1/phand_knuckle11/phand_fingerpart 1
    m_Weight: 0
  - m_Path: l_hand_world/b_l_hand/phand_thumbbase
    m_Weight: 0
  - m_Path: l_hand_world/pDuck
    m_Weight: 1
  - m_Path: l_hand_world/pHand_base
    m_Weight: 0
  - m_Path: l_hand_world/pHand_base/phand_palm
    m_Weight: 0
