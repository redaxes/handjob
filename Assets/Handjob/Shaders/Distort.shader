﻿Shader "Custom/Distort"
{
    Properties{
        _DistortColor("Distort Color", Color) = (1,1,1,1)
        _BumpAmt("Distortion",Range(0,128)) = 10
        _DistortTex("Distortion Texture (RBG)", 2D) = "white" {}
        _BumpMap("Bump Map", 2D) = "bump" {}
    }
    
    SubShader
    {
    
        //We can have any number of passes
        //Each pass is one "Render Pass", means rendering it once
    
        Tags
        {
            "Queue" = "Transparent"
        }
        
        //It's going to grab the object (we get the vertex data right) and makes a pass retrieving data about the objects BEHIND our object
        //We later access data from here in out Passes (for example sampler2D _GrabPass)
        GrabPass{}
        
        //Pass for the Horizontal Blur
        Pass{
            //Name for if we want to use it in another function
            Name "DISTORT"
            
            CGPROGRAM //Allows talk between two languages: shader lab and nvidia C for graphics.
                
                //Every program needs a vertex function and a fragment function
                
                //define for building function (shape)
                #pragma vertex vert
                //define for coloring function
                #pragma fragment frag
                
                //Includes
                //Built-in Shader Functions by Unity
                #include "UnityCG.cginc"
                
                //Structures on how things get data
                
                struct appdata{
                    float4 vertex : POSITION;
                    float2 texcoord : TEXCOORD0;
                };
                
                //v2f is how the fragment function gets its data
                //use SV_POSITION instead of POSITION so it works on every platform
                struct v2f{
                    float4 vertex : SV_POSITION;
                    float4 uvgrab : TEXCOORD0;
                    float2 uvbump : TEXCOORD1;
                    float uvmain : TEXCOORD2;
                };
                
                //IMPORTS
                
                float _BumpAmt;
                float4 _BumpMap_ST;
                float4 _DistortTex_ST;
                
                fixed4 _DistortColor;
                sampler2D _GrabTexture;
                float4 _GrabTexture_TexelSize;
                sampler2D _BumpMap;
                sampler2D _DistortTex;
                
                //Vertex Function
                v2f vert(appdata IN){
                    v2f OUT;
                    
                    OUT.vertex = UnityObjectToClipPos(IN.vertex);
                    
                    #if UNITY_UV_STARTS_AT_TOP
                        float scale = -1;
                    #else
                        float scale = 1;
                    #endif
                    
                    OUT.uvgrab.xy = (float2(OUT.vertex.x,OUT.vertex.y*scale) + OUT.vertex.w) * 0.5;
                    OUT.uvgrab.zw = OUT.vertex.zw;
                    
                    OUT.uvbump = TRANSFORM_TEX(IN.texcoord,_BumpMap);
                    OUT.uvmain = TRANSFORM_TEX(IN.texcoord,_DistortTex);
                    
                    return OUT;
                }
                
                //Fragment function
                half4 frag(v2f IN) : COLOR
                {
                    half2 bump = UnpackNormal(tex2D(_BumpMap,IN.uvbump)).rg;
                    float2 offset = bump * _BumpAmt * _GrabTexture_TexelSize.xy;
                    
                    IN.uvgrab.xy = offset * IN.uvgrab.z + IN.uvgrab.xy;
                    
                    half4 col = tex2Dproj(_GrabTexture,UNITY_PROJ_COORD(IN.uvgrab));
                    half4 tint = tex2D(_DistortTex,IN.uvmain)* _DistortColor;
                    
                    return col * tint;
                }
                
            ENDCG
        }    
        
    }
    
}
