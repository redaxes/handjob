﻿Shader "Custom/OutlineBlur"
{

    //GAUSSIAN BLUR

    Properties{
        _BlurRadius("Blur Radius", Range(0,20)) = 1
        _Intensity("Blur Intensity", Range(0,1)) = 0.01
        _OutlineWidth("Outline Width", Range(1,10)) = 1.1
    }
    
    SubShader
    {
    
        //We can have any number of passes
        //Each pass is one "Render Pass", means rendering it once
    
        Tags
        {
            "Queue" = "Transparent"
        }
        
        //It's going to grab the object (we get the vertex data right) and makes a pass retrieving data about the objects BEHIND our object
        //We later access data from here in out Passes (for example sampler2D _GrabPass)
        GrabPass{}
        
        //Pass for the Horizontal Blur
        Pass{
            //Name for if we want to use it in another function
            Name "OUTLINEHORIZONTALBLUR"
            
            ZWrite Off
            
            CGPROGRAM //Allows talk between two languages: shader lab and nvidia C for graphics.
                
                //Every program needs a vertex function and a fragment function
                
                //define for building function (shape)
                #pragma vertex vert
                //define for coloring function
                #pragma fragment frag
                
                //Includes
                //Built-in Shader Functions by Unity
                #include "UnityCG.cginc"
                
                //Structures on how things get data
                
                //We do not need appdata here, we can just use appdata_base in our vert as its already defined
                
                //v2f is how the fragment function gets its data
                //use SV_POSITION instead of POSITION so it works on every platform
                struct v2f{
                    float4 vertex: SV_POSITION;
                    float4 uvgrab : TEXCOORD0;
                };
                
                //IMPORTS
                
                float _BlurRadius;
                float _Intensity;
                //Texture that comes from the GrabPass
                sampler2D _GrabTexture;
                float4 _GrabTexture_TexelSize;
                float _OutlineWidth;
                
                //Vertex Function
                // v2f is what it returns, the input for frag, vert is the name of the function we defined earlier and appdata is the struct we also defined earlier
                v2f vert(appdata_base IN){
                    v2f OUT;
                    
                    IN.vertex.xyz *= _OutlineWidth + 0.1;
                    
                    OUT.vertex = UnityObjectToClipPos(IN.vertex);
                    
                    #if UNITY_UV_STARTS_AT_TOP
                        float scale = -1;
                    #else
                        float scale = 1;
                    #endif
                    
                    OUT.uvgrab.xy = (float2(OUT.vertex.x,OUT.vertex.y*scale) + OUT.vertex.w) * 0.5;
                    OUT.uvgrab.zw = OUT.vertex.zw;
                    
                    return OUT;
                }
                
                //Fragment function
                half4 frag(v2f IN) : COLOR
                {               
                    half4 texColor = tex2Dproj(_GrabTexture,UNITY_PROJ_COORD(IN.uvgrab));
                    half4 texsum = half4(0,0,0,0);
                    
                    //Horizontal Pass for gaussian BLUR
                    
                    //We define the Function for the horizontal blur pass
                    #define GRABPIXEL(weight, kernalx) tex2Dproj(_GrabTexture,UNITY_PROJ_COORD(float4(IN.uvgrab.x + _GrabTexture_TexelSize.x * kernalx * _BlurRadius, IN.uvgrab.y, IN.uvgrab.z,IN.uvgrab.w))) * weight
                    
                    texsum += GRABPIXEL(0.05,-4);
                    texsum += GRABPIXEL(0.09,-3);
                    texsum += GRABPIXEL(0.12,-2);
                    texsum += GRABPIXEL(0.15,-1);
                    texsum += GRABPIXEL(0.18,0);
                    texsum += GRABPIXEL(0.15,1);
                    texsum += GRABPIXEL(0.12,2);
                    texsum += GRABPIXEL(0.09,3);
                    texsum += GRABPIXEL(0.05,4);
                    
                    texColor = lerp(texColor,texsum,_Intensity);
                    
                    return texColor;
                }
                
            ENDCG
        }    
        
        GrabPass{}
        //This pass grabs also the newly blurred stuff
        
       //Vertical Blur Pass
        Pass{
            Name "OUTLINEVERTICALBLUR"
            
            ZWrite Off
            
            CGPROGRAM //Allows talk between two languages: shader lab and nvidia C for graphics.
                
                //Every program needs a vertex function and a fragment function
                
                //define for building function (shape)
                #pragma vertex vert
                //define for coloring function
                #pragma fragment frag
                
                //Includes
                //Built-in Shader Functions by Unity
                #include "UnityCG.cginc"
                
                //Structures on how things get data

                //v2f is how the fragment function gets its data
                //use SV_POSITION instead of POSITION so it works on every platform
                struct v2f{
                    float4 vertex: SV_POSITION;
                    float4 uvgrab : TEXCOORD0;
                };
               
               //IMPORTS
                
                float _BlurRadius;
                float _Intensity;
                //Texture that comes from the GrabPass
                sampler2D _GrabTexture;
                float4 _GrabTexture_TexelSize;
                float _OutlineWidth;
                
                //Vertex Function
                // v2f is what it returns, the input for frag, vert is the name of the function we defined earlier and appdata is the struct we also defined earlier
                v2f vert(appdata_base IN){
                    v2f OUT;
                    
                    IN.vertex.xyz *= _OutlineWidth + 0.1;
                    
                    //Takes the object from the object space and puts it into camera clip space
                    OUT.vertex = UnityObjectToClipPos(IN.vertex);
                    
                    #if UNITY_UV_STARTS_AT_TOP
                        float scale = -1;
                    #else
                        float scale = 1;
                    #endif
                    
                    OUT.uvgrab.xy = (float2(OUT.vertex.x,OUT.vertex.y*scale) + OUT.vertex.w) * 0.5;
                    OUT.uvgrab.zw = OUT.vertex.zw;
                    
                    return OUT;
                }
                
                //Fragment function
                //SV_Target : System-Value Target, just specifying that it is a Rendertarget, more info : https://cgcookie.com/questions/8160-after-watching-the-video-there-were-a-few-points-i-wanted-to-clarify-if-that-was-ok
                half4 frag(v2f IN) : COLOR
                {
                    half4 texColor = tex2Dproj(_GrabTexture,UNITY_PROJ_COORD(IN.uvgrab));
                    half4 texsum = half4(0,0,0,0);
                    
                    //Horizontal Pass for gaussian BLUR
                    
                    //We define the Function for the horizontal blur pass
                    #define GRABPIXEL(weight, kernaly) tex2Dproj(_GrabTexture,UNITY_PROJ_COORD(float4(IN.uvgrab.x, IN.uvgrab.y + _GrabTexture_TexelSize.y * kernaly * _BlurRadius, IN.uvgrab.z,IN.uvgrab.w))) * weight
                    
                    texsum += GRABPIXEL(0.05,-4);
                    texsum += GRABPIXEL(0.09,-3);
                    texsum += GRABPIXEL(0.12,-2);
                    texsum += GRABPIXEL(0.15,-1);
                    texsum += GRABPIXEL(0.18,0);
                    texsum += GRABPIXEL(0.15,1);
                    texsum += GRABPIXEL(0.12,2);
                    texsum += GRABPIXEL(0.09,3);
                    texsum += GRABPIXEL(0.05,4);
                    
                    texColor = lerp(texColor,texsum,_Intensity);
                    
                    return texColor;
                }
                
            ENDCG
        }    
    }
    
}
