﻿Shader "Custom/FinalOutline_Unlit"
{
    Properties{
    
        //Unlit Properties
        _MainTex("Main Texture (RGB)", 2D) = "white" {}
        _Color("Color",Color) = (1,1,1,1)
        _OutlineWidth("Outline Width", Range(1,10)) = 1.1
        
        //Blur Properties
        _BlurRadius("Blur Radius", Range(0,20)) = 1
        _Intensity("Blur Intensity", Range(0,1)) = 0.01
        
        //Distortion Properties
        _DistortColor("Distort Color", Color) = (1,1,1,1)
        _BumpAmt("Distortion",Range(0,128)) = 10
        _DistortTex("Distortion Texture (RBG)", 2D) = "white" {}
        _BumpMap("Bump Map", 2D) = "bump" {}
    }
    
    SubShader
    {
        Tags{
            "Queue" = "Transparent"
        }
    
        //We can call the other shader passes from outline, blur and Distort
        
        //Grab behind object
        GrabPass{}
        UsePass "Custom/OutlineDistort/OUTLINEDISTORT"
        //Grab the Distort
        GrabPass{}
        UsePass "Custom/OutlineBlur/OUTLINEHORIZONTALBLUR"
        //Grab both
        GrabPass{}
        UsePass "Custom/OutlineBlur/OUTLINEVERTICALBLUR"
         
        UsePass "Custom/Outline/OBJECT"
    }
    
}
