﻿Shader "Custom/FinalOutline"
{
    Properties{
    
        //Unlit Properties
        _MainTex("Main Texture (RGB)", 2D) = "white" {}
        _Color("Color",Color) = (1,1,1,1)
        _OutlineWidth("Outline Width", Range(1,10)) = 1.1
        
        //Blur Properties
        _BlurRadius("Blur Radius", Range(0,20)) = 1
        _Intensity("Blur Intensity", Range(0,1)) = 0.01
        
        //Distortion Properties
        _DistortColor("Distort Color", Color) = (1,1,1,1)
        _BumpAmt("Distortion",Range(0,128)) = 10
        _DistortTex("Distortion Texture (RBG)", 2D) = "white" {}
        _BumpMap("Bump Map", 2D) = "bump" {}
        
        //All the rest for Standard
         _Cutoff ("Alpha Cutoff", Range(0.000000,1.000000)) = 0.500000
         _Glossiness ("Smoothness", Range(0.000000,1.000000)) = 0.500000
         _GlossMapScale ("Smoothness Scale", Range(0.000000,1.000000)) = 1.000000
        [Enum(Metallic Alpha,0,Albedo Alpha,1)]  _SmoothnessTextureChannel ("Smoothness texture channel", Float) = 0.000000
        [Gamma]  _Metallic ("Metallic", Range(0.000000,1.000000)) = 0.000000
         _MetallicGlossMap ("Metallic", 2D) = "white" { }
        [ToggleOff]  _SpecularHighlights ("Specular Highlights", Float) = 1.000000
        [ToggleOff]  _GlossyReflections ("Glossy Reflections", Float) = 1.000000
         _BumpScale ("Scale", Float) = 1.000000
        [Normal]  _BumpMap ("Normal Map", 2D) = "bump" { }
         _Parallax ("Height Scale", Range(0.005000,0.080000)) = 0.020000
         _ParallaxMap ("Height Map", 2D) = "black" { }
         _OcclusionStrength ("Strength", Range(0.000000,1.000000)) = 1.000000
         _OcclusionMap ("Occlusion", 2D) = "white" { }
         _EmissionColor ("Color", Color) = (0.000000,0.000000,0.000000,1.000000)
         _EmissionMap ("Emission", 2D) = "white" { }
         _DetailMask ("Detail Mask", 2D) = "white" { }
         _DetailAlbedoMap ("Detail Albedo x2", 2D) = "grey" { }
         _DetailNormalMapScale ("Scale", Float) = 1.000000
        [Normal]  _DetailNormalMap ("Normal Map", 2D) = "bump" { }
        [Enum(UV0,0,UV1,1)]  _UVSec ("UV Set for secondary textures", Float) = 0.000000
        [HideInInspector]  _Mode ("__mode", Float) = 0.000000
        [HideInInspector]  _SrcBlend ("__src", Float) = 1.000000
        [HideInInspector]  _DstBlend ("__dst", Float) = 0.000000
        [HideInInspector]  _ZWrite ("__zw", Float) = 1.000000
    }
    
    SubShader
    {
        Tags{
            "Queue" = "Transparent"
        }
    
        //We can call the other shader passes from outline, blur and Distort
        
        //Grab behind object
        GrabPass{}
        UsePass "Custom/OutlineDistort/OUTLINEDISTORT"
        //Grab the Distort
        GrabPass{}
        UsePass "Custom/OutlineBlur/OUTLINEHORIZONTALBLUR"
        //Grab both
        GrabPass{}
        UsePass "Custom/OutlineBlur/OUTLINEVERTICALBLUR"
         
        UsePass "Standard/FORWARD"
        
        //This seemingly does nothing
        //UsePass "Standard/FORWARD_DELTA"
        
    }
    
}
